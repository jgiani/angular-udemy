# Inhalt
# Abschnitt 1: Einführung und Installation

## 1: Einführung
## 2: Installation der benötigten Tools
Node.js und Editor wie VS-Code installieren.
# Abschnitt 2: Erste Schritte
## 3: Einleitung
## 4: Hintergrundwissen
Das Einstiegsskript in ***index.html*** ist

`system.config.js`

Dieses läd im angegebenen Ordner ***app*** die Datei

`main.js`

Von hieraus wird ein *Modul* geladen:

`app.module.js`

Dieses *Modul* läd wiederum ein *Komponente*

`app.component.js`

## 5: Ausgeben von Variablen
Ausgabe von Variablen mit
```typescript
    <h1>{{ someVariable }}</h1>
```
Muss definiert sein in der Komponenten-Klasse
```typescript
    export class AppComponent {
        someVariable = "Hallo";
    }
```    
Mehrzeiliges HTML mit ``:
```typescript
    @Component({
        selector: 'my-app',
        template: `
            <h1> {{ title }} </h1>
            <p> Hallo du! </p>
        `
    })
```
## 6: Variablen ändern / this / Arrowfunctions
Referenzieren des umgebenden `this` innerhalb einer (Callback)-Funktion mit Arrowfunctions:

```typescript
    export class AppComponent {
        someVariable :string;
        constructor() {
            someVariable = "Hallo Welt!";
            setTimeout(() => {
                this.someVariable = "Hallo Du!";
            }, 2500);
        }
    }
```

## 7: `*ngFor`, Konstruktur-Kurzschreibweise und Auslagern von Klassen
Im Template über Array iterieren mit `*ngFor`:

```typescript
    @Component({
        selector: 'my-ul-students',
        template: `
        <ul>
            <li *ngFor="let student of students">{{ student.lastname }}</li>
        </ul>
        `
    })

export class AppComponent {
    students = [
        {firstname : "Eric", lastname : "Tyson"},
        {firstname : "Samira", lastname : "Schwartz"},
        {firstname : "Max", lastname : "Goethe"}
    ];
}
```

### Kurzschreibweise für Konstruktoren
Kurzschreibweise für Deklaration von Klassenvariablen. Anstatt

```typescript
        class Student {
        public firstname: string;
        public lastname: string;

        constructor(firstname: string,lastname: string) {
            this.firstname = firstname;
            this.lastname = lastname;
        }
    }
```

kann die Deklaration auch **innerhalb** des Konstruktors stattfinden:

```typescript
    class Student {
        constructor(public firstname: string,public lastname: string) {
            this.firstname = firstname;
            this.lastname = lastname;
        }
    }
```

### Auslagern von Klassen
Als Regel sollte gelten: **Eine** Klasse pro Datei. 
Die Klasse `Student` sollte deshalb ausgelagert werden.
Damit diese von anderen Komponenten/Klassen/... verwendet werden kann muss die Modifier `export` gesetzt sein:

```typescript
    export class Student {
        constructor(public firstname: string,public lastname: string) {
            this.firstname = firstname;
            this.lastname = lastname;
        }
    }
```

Der Import funktioniert über ein Import-Statement unter Angabe des Pfads:

```typescript
    import { Student } from "./student";
```
## 8: Typsicherheit in TypeScript
TypeScript bringt Typsicherheit zur Kompilizerzeit. 
D.h. das generierte JavaScript prüft nicht mehr auf Typen.

+ String
+ Number
+ Boolean
+ Array
+ Tuple
+ Enum
+ Any

Beim Typ `any` wird die Typsicherheit ausgeschaltet. 
Deshalb sollte dies mit Vorsicht genutzt werden

# Abschnitt 3: Grundlegende Funktionen

## 9: Reagieren auf Events
```typescript

    <button (click)="onAddStudent()">Student hinzufügen</button>

```

Mit `*` wie bei `*ngFor` werden *Strukturveränderungen* vorgenommen. Hingegen bedeutet `()` wie bei `(click)`, dass auf ein *Event* gelauscht wird.

***Bemerkung:*** Mit dem clicken wird direkt der Teil des DOMs manipuliert, welcher mit `*ngFor` aufgebaut wurde (Die Liste).

## 10: ngFor mit Index

`index` ist eine reservierte Veriable, in der man innerhalb von `*ngFor`zugreifen kann. Hierüber kann der index einer Variablen zugewiesen werden, die dann im weiteren Teil des Templates genutzt werden kann.

```typescript

        <li *ngFor="let student of students; let i = index">
        {{ i }}: {{ student.firstname }} <button (click)="onRemoveStudent(i)">Remove</button>
        </li>
```

## 11/12: Das keyup-event

### Die Variable `$event`
Die reservierte Variable `$event` lässt sich nutzen, um Details über das Ereignis zu erfahren.

```typescript
    <input type="text" (keyup)="onInputChange($event)"/>
```
### Typecasting

Typecasting funktioniert über `<>`:

```typescript
    onInputChange(event: KeyboardEvent): void {
        let target = <HTMLInputElement> event.target;
        this.inputValue = target.value;
    }
```
### Kompatibilität zu nativen Apps

Mit der vorherigen Implementierung wird auf HTML-spezifische Ereignisse (Das `event: KeyboardEvent`) zurückgegriffen. Damit die Anwendung später auch für native Android- oder iOS-Apps funktioniert, sollte das vermieden werden. Anstelle dessen bietet sich folgende Alternative an. 

Im Template wird direkt die `value` des Inputfelds übermittelt:

```typescript
    <input type="text" (keyup)="onInputChange($event.target.value)"/>
```

Die Funktion `onInputChange` kann dadurch vom Typecasting befreift werden. Der Funktion wird nicht das Event, sondern direkt ein String übergeben:

```typescript
    onInputChange(inputValue: string): void {
        this.inputValue = inputValue;
    }
```

## 13/14: `*ngIf`

Mit `*ngIf` können HTML-Elemente gekoppelt an Bedingungen verändert werden. Nur wenn im Beispiel kein Leerstring exisitert wird das `p`-Element angezeigt:

```typescript
    <p *ngIf="inputValue != ''"> Im Eingabefeld steht: {{ inputValue }}</p>
```

# Abschnitt 4: Components, Components, Components

## 15: Components und HTML-Tags

Angular erlaubt es eigene HTML-Tags wie `<my-card>` zu definieren. `<my-card>` dient dann in der dazugehörigen `card.component.ts` als `selector`.

```typescript
    @Component({
        selector: 'my-card',
        template: `<div style="border: 1px solid black;">
        <p>MyCard</p></div>` 
    })
```

## 16: Übergabe von Tag-Attributen

Im Template von `app.component.ts` möchten wir `<my-card>` nun nutzen und einen Paramter für das eigens angelegte Attribut `title` übergeben:

```html
    <my-card title="Das ist der Titel"><my-card>
```

Um auf das Attribut `title` zugreifen zu können bietet Angular die `Input`-Annotation an. Damit lässt sich die Variable `title` im Template wie gewohnt über `{{ title }}` nutzen:

```typescript
import { Input } from '@angular/core';

@Component({
    selector: 'my-card',
    template: `<div style="border: 1px solid black;">
    <h1>{{ title }}</h1>
    <p>MyCard</p></div>` 
})

export class CardComponent {
    @Input() title: string;
}
```

## 17: Übergabe von Objekten als Parameter

Die Übergabe von Objekten ist über Attribute nur bedingt möglich. Um Objekte übergeben zu können, sind `[]` als Schreibweise definiert.

```typescript
@Component({
    selector: 'my-app',
    template: `<div *ngFor="let card of cards">
    <my-card title="{{ card.title }}" 
    [date]="card.date"></my-card>
    </div>`
})

export class AppComponent {

    cards = [
        {
            title: "Title A",
            date: new Date(2018,1,1),
        },
        {
            title: "Title BBB",
            date: new Date(2019,21,11),
        }
    ];

}
```

In `CardComponent` ist das `date` entsprechend als Klassenvariable vorhanden:

```typescript
export class CardComponent {
    @Input() title: string;
    @Input() date: Date;
}
```

### Property Binding
***Anmerkung: Dies wird so nicht im udemy-Kurs beschrieben, scheint aber hilfreich für das weitere Verständnis.***

Allgemein wird diese Form des Bindings über `[]` als *Property Binding* bezeichnet. *Property Binding* kann auch bspw. dazu genutzt werden das `src`-Attribut von `<img>` zu setzen:

```typescript
@Component({
    selector: 'my-app',
    template: `<img [src]="sourceImage"`
})

export class AppComponent {
    sourceImage: string = "img.jpg";
}
```

Damit wird der View/das Template an Variable `sourceImage` in der Anwendungslogik gebunden.

## 18: Eigene Events

Im Folgenden wird beschrieben, wie eine Stoppuhr herunterläuft und bei erreichen des Werts 0 ein eigens definiertes Ereignis emittiert wird.

Mit dem Attribut `countdown` wird der Startwert gesetzt. Außerdem wird mit `(countdownExceeded)` beschrieben, dass das auf dieses Event gelauscht und bei Auftreten die Funktion `onCountdownExceeded()` aufgerufen wird.

```typescript
    <my-timer countdown="1" (countdownExceeded)="onCountdownExceeded()"></my-timer>
```

In `TimerComponent` ist zum emittieren des Events `countdownExceeded` ein entsprechendes Objekt als `@Output`/`EventEmitter` implementiert.
Über die Funktion `emit()` kann das Event in Umlauf gebracht werden und darauf entsprechend reagiert werden (siehe oben).

```typescript
@Component({
    selector:'my-timer',
    template:`<p> {{ countdown }} </p>`

})

export class TimerComponent {
    @Input() countdown: number;
    @Output() countdownExceeded = new EventEmitter();
    constructor() {
        setInterval(() => {
            this.countdown--;
            if (this.countdown == 0) {
                this.countdownExceeded.emit();
            }
        }, 1000);
    }
}
```

## 19-21: Der Event Emitter

Die in `subscribe()` definierte Funktion wird ausgeführt, wann immer das Event ausgelöst wird:

```typescript
let eventEmitter = new EventEmitter();
eventEmitter.subscribe(function() {
    //code to be excecuted when event occurs
});

```

Mit `emit()` wird das Event ausgelöst:

```typescript
eventEmitter.emit();
```

`emit(parameter)` funktioniert auch mit Paramtern, sodass die in `subscribe` definierte Funktion diesen entgegen nehmen kann.

```typescript
eventEmitter.subscribe(function(param: any) {
    console.log(param);
});
eventEmitter.emit("Hallo");
```

***Das subscriben funktioniert automatisch, wenn mit der Schreibweise*** `(event)` ***innerhalb eines Tags. Genau das ist im Beispiel oben mit*** `(countdownExceeded)` ***geschehen.***

```typescript
    <my-timer countdown="1" (countdownExceeded)="onCountdownExceeded()"></my-timer>
```

# Abschnitt 5: Parameter übergeben (data-binding) 
## 22: Two-way Data-binding mit `ngModel`

Mit der Anweisung `[(ngModel)]` wird eine Kombination von Data-bindings geschaffen, die es ermöglicht, sowohl geänderte Daten abzugreifen, als auch Änderungen an Daten zu propagieren. Beispiel: Eingabe aus einem Inputfeld soll in einem Paragraph gleichzeitig erscheinen:

```typescript
    <input type="text" value="{{ theTitle }}" (keyup)="theTitle = $event.target.value"/>
    <p>{{ theTitle }}</p>
```

Diese Schreibweise ist umständlich und kann einfacher gestaltet werden mit `[(ngModel)]`:


```typescript
    <input type="text" [(ngModel)]="theTitle"/>
    <p>{{ theTitle }}</p>
```

 Für `[(ngModel)]` muss das `FormsModule` für ***das dazugehörige/umgebende Modul*** geladen werden. Es handelt sich bei `[(ngModel)]`  also um eine Anweisung, die explizit mit der Verarbeitung von Formularen in Verbindung steht. Genauer gesagt bindet `[(ngModel)]` das `value`-Attribut eines `input`-Felds an eine Variable, und das Binding ist dann ***bidirektional***. Das bedeutet, wenn sich der View/das Template ändert, ändert sich auch das Model. Das ist genau der entgegengesetzte Weg im Vergleich zum *Property Binding*.

 ```typescript
    import { FormsModule } from '@angular/forms';

    @NgModule({
    imports: [ BrowserModule, FormsModule ],
    declarations: [
        AppComponent,
    ],
    bootstrap: [ AppComponent ]
    })
    export class AppModule { }
```

## 23/24: `ngModel` im Detail

Folgende Schreibweisen sind äquvialent:

 ```typescript
    <input type="text" [(ngModel)]="theTitle">
```

 ```typescript
    <input type="text" [ngModel]="theTitle" (ngModelChange)="theTitle = $event">
```

`ngModel` ist demnach ein Event Emitter, welches das Ereignis `ngModelChange` auswirft. D.h. es ist auch möglich auf dieses Event zu lauschen:

 ```typescript
    <input type="text" [(ngModel)]="theTitle" (ngModelChange)="console.log('the title changed')">
```

## 25/26: Beispielanwendung: Todo-Liste
Auf die Beispielanwendung wird hier nicht eingegangen

# Abschnitt 6: Components (2)
## 27: CSS-Klassen mit `[ngClass]` und `[class.]` setzen
Mit `[ngClass]` können Klassen abhängig von der Anwendungslogik vergeben werden:

 ```typescript
    <p [ngClass]="'active':isActive()">The Content</p>
```

Wenn `ìsActive()` den Wert `true` liefert, ist die CSS-Klasse `active` gesetzt, andernfalls nicht. Alterntiv bietet es sich an auch eine Funktion zu schreiben, die mehrere Klasen zurück gibt:


 ```typescript
    <p [ngClass]="'getClasses()">The Content</p>
```

In der Komponente wird dann ein Funktion angelegt, die ein Objekt aller Klassen zurückgibt. Von dieser Schreibweise ist jedoch abzuraten, da damit im Funktionscode auf das Styling eingewirkt wird. Bei Änderungen im CSS-Framework müsste dies dann nicht im Template-Code sondern im Funktionscode abgeändert werden.

```typescript
export class TheComponent {
    getClasses() {
        return {
            'list-group-item':true,
            'list-group-item-success':false,
        }
    }
}
```

Eine weitere Schreibweise lautet wie folgt:

 ```typescript
    <p [class.list-group-item-success]="aProperty == 'stringToCompare'">The Content</p>
```

Wenn `aProperty` gleich dem zu vergleichenden String `stringToCompare` ist liefert der Ausdruck `true` und die `list-group-item-success` Klase ist gesetzt. Damit lassen sich recht übersichtlich mehrere Klassen setzen, da pro Kasse ein `[class....]` als Attribut im HTML beschrieben ist.

## 29: CSS-Styles `[ngStyle]` und `[style.]` setzen

Ähnlich wie mit `[class.]` lassen sich mit `[style.]` CSS-Style-Attribute manipulieren.

```typescript
    <p [style.font-weight]="'bold'">The Content</p>
```

Analog zu`[ngClass]` gibt es auch `[ngStyle]`:

```typescript
    <p [ngStyle]="'font-weight': anotherProperty">The Content</p>
```

`anotherProperty` liefert dann etwas wie `bold`.

## 30: Components: Wiederverwendbarkeit mit `<ng-content>` 

Bisher wurde einer Komponente ein Parameter übergeben, auf den sie mit `@Input` zugreifen kann z.B. so für die übergoerdnete Komponente

```typescript
    <my-comp title="The Title"></my-comp>
```

Entsprechend im Template der eigenen Komponenten ist dies dann wie folgt definiert:

```typescript
@Component({
    selector:'my-comp'
    template:'<p>{{ title }}</p>'
})

export class CompComponent {
    @Input title: String;
}
```

So kann der Inhalt innerhalb der Komponente `MyComp` im Template-Code manipuliert werden. Auffällig: bisher ist nie ein Inhalt in `<my-comp></my-comp>` angegeben gewesen. Mit `<ng-Content>` lässt sich dies bewerkstelligen.

```typescript
@Component({
    selector:'my-comp'
    template:`
    <div>
        <h1>{{ title }}</h1>
        <ng-content></ng-content>
    </div>
    `
})
```

Somit können wiederverwendbare Komponenten geschaffen werden, wie z.B. Panels deren Inhalt variiert:

```typescript
    <my-comp title="The Title">
        <ul class="list-group">
            <li *ngFor="let obj of objects">
                {{ obj.property }}
            </li>
        </ul>
    </my-comp>
```

Innerhalb von `<my-comp></my-comp>` wurde nun weiterer Template-Code beschrieben. Dieser wird dann in der der Komponente `MyComp` dort gerendert, wo  `<ng-content></ng-content>` im Template-Code steht.

## 30: Components: Noch mehr Wiedervwendbarkeit mit `<ng-content select="...">` 
 
 Über das `select`-Attribut in `<ng-content>` können innerhalb einer Komponente weitere Templates eingebunden werden, z.B. eine für den Header-, Body- und Footer-Bereich:


```typescript
@Component({
    selector:'my-comp'
    template:`
    <div>
        <ng-content select="my-comp-header"></ng-content>
        <ng-content select="my-comp-body"></ng-content>
        <ng-content select="my-comp-footer"></ng-content>
    </div>
    `
})
```

Dazu benötigt werden die jeweiligen Komponenten `comp-header`, `comp-body` und `comp-footer` als jeweilige `.component.ts`-Files mit entsprechendem Template-Code innerhalb von `@Componet({ template : ... })` und ausgewiesenem Selector (z.B.: `selector:"my-comp-header"`). Diese müssen in `app.module.ts` auch entsprechend wieder geladen und in `declarations` eingebunden werden:

```typescript
//Weitere Imports...
import { CompComponent } from './comp.component';
import { CompHeaderComponent } from './comp-header.component';
import { CompBodyComponent } from './comp-body.component';
import { CompFooterComponent } from './comp-footer.component';

@NgModule({
    //imports, bootstrap, ...
  declarations: [
    AppComponent,
    CompComponent,
    CompHeaderComponent,
    CompBodyComponent,
    CompFooterComponent,
   ],
})
```

In `AppComponent` kann jetzt die wiederverwendbare `CompCompent` wie folgt genutzt und mit Inhalt befüllt werden:

```typescript
    template: `
     <my-comp>
        <my-comp-header>
            This is the Header of <em>MyComp</em>
        </my-comp-header>
        <my-comp-body>
            <ul class="list-group">
                <li *ngFor="let obj of objects">
                    {{ obj.property }}
                </li>
            </ul>
        </my-comp-body>
        <my-comp-footer>
            ...
        </my-comp-footer>
     </my-comp>
    `
```

