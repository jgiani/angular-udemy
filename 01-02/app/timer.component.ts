import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector:'my-timer',
    template:`<p> {{ countdown }} </p>`

})

export class TimerComponent {
    @Input() private countdown: number;
    @Output() countdownExceeded = new EventEmitter();
    constructor() {
        setInterval(() => {
            this.countdown--;
            if (this.countdown == 0) {
                this.countdownExceeded.emit();
            }
        }, 1000);
    }
}