import { Component } from '@angular/core';

@Component({
    selector : 'my-photo',
    template : `
        <input type="text" [(ngModel)]="name" (ngModelChange)="nameChanged()"/>
        <input type="text" [(ngModel)]="otherName" (ngModelChange)="otherNameChanged()"/>
        <p>{{ name }}</p>
    `
})

export class PhotoComponent {
    name: string = "MyName";
    otherName: string = "my other name";


    nameChanged(): void {
        console.log("MyName changed");
    }

    otherNameChanged(): void {
        console.log("my other name changed");
    }
}

