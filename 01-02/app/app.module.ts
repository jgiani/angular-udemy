import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent }  from './app.component';
import { CardComponent } from './card.component';
import { TimerComponent } from './timer.component';
import { PhotoComponent } from './photo.component';


@NgModule({
  imports: [ BrowserModule, FormsModule ],
  declarations: [
    AppComponent,
    CardComponent,
    TimerComponent,
    PhotoComponent
   ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
