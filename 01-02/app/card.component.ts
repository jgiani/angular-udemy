import { Component, Input } from '@angular/core';

@Component({
    selector: 'my-card',
    template: `
    <div style="border: 1px solid black;">
    <h1>{{ title }}</h1>
    <b>{{ date.getFullYear() }}</b>
    <p>MyCard</p>
    </div>
    ` 
})

export class CardComponent {
    @Input() title: string;
    @Input() date: Date;
}

