import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
     <my-photo></my-photo>
    `
})

export class AppComponent {

    cards = [
        {
            title: "Title A",
            date: new Date(2018,1,1),
        },
        {
            title: "Title BBB",
            date: new Date(2019,21,11),
        }
    ];

    onCountdownExceeded(): void {
        alert("Exceeded bitch!");
    }

}
